using System.Collections;
using UnityEngine;

public class SatelliteSpawner : MonoBehaviour {

	public GameObject meteorPrefab;
	public float distance = 25f;

	void Start ()
	{
		StartCoroutine(SpawnSatellite());
	}

	IEnumerator SpawnSatellite()
	{
		Vector3 pos = Random.onUnitSphere * 20f;
		Instantiate(meteorPrefab, pos, Quaternion.identity);

		yield return new WaitForSeconds(1f);

		StartCoroutine(SpawnSatellite());
	}

}
